package ants.mobile.ants_insight.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import ants.mobile.ants_insight.Anonymous;
import ants.mobile.ants_insight.Constants.Constants;
import ants.mobile.ants_insight.Constants.Events;
import ants.mobile.ants_insight.CDP365AnalyticSDK;
import ants.mobile.ants_insight.InsightSharedPref;

import static ants.mobile.ants_insight.Constants.Constants.PREF_IS_FIRST_INSTALL_APP;
import static ants.mobile.ants_insight.Constants.Constants.PREF_ANDROID_APP_PUSH_ID;

public class InsightDataRequest {
    private List<ProductItem> productItemList;
    private ContextModel contextModel;
    private ExtraItem extraItem;
    public Events eventAction;
    public String eventCategory;
    public String eventActionName;
    private List<Dimension> dimensionList;
    private static final Long INTERVAL_REFRESH_SECTION = 1800000L; // milli second
    private Context mContext;
    private UserItem userItem;
    private static String sections = "";
    public static boolean isCustomEvent =false;

    public static class Builder {
        private Events eventName;
        private List<ProductItem> productList;
        private UserItem userItem;
        private ExtraItem extraItem;
        private List<Dimension> dimensionList;
        private Campaign campaign;
        private Context context;

//        public Builder eventCategoryCustom(String eventCategoryCustom) {
//            this.eventCategoryCustom = eventCategoryCustom;
//            return this;
//        }

        public Builder user(UserItem userItem) {
            this.userItem = userItem;
            return this;
        }

        public Builder dimensionList(List<Dimension> dimensionList) {
            this.dimensionList = dimensionList;
            return this;
        }

        public Builder extraData(ExtraItem item) {
            this.extraItem = item;
            return this;
        }

        public Builder eventName(Events eventName) {
            this.eventName = eventName;
            return this;
        }

        public Builder productList(List<ProductItem> productList) {
            this.productList = productList;
            return this;
        }

        public Builder setCampaign(Campaign campaign) {
            this.campaign = campaign;
            return this;
        }

        public Builder setInstance(Context context) {
            this.context = context;
            return this;
        }

        public InsightDataRequest build() {
            return new InsightDataRequest(this);
        }
    }

    private InsightDataRequest(Builder builder) {
        this.mContext = builder.context;
        this.contextModel = new ContextModel(builder.context);
        this.eventAction = builder.eventName;
        this.productItemList = builder.productList;
        this.dimensionList = builder.dimensionList;
        this.userItem = builder.userItem;
        this.extraItem = builder.extraItem;
//        this.eventCategoryCustom = builder.eventCategoryCustom;
        this.setCampaign(builder.campaign);
            if (eventAction != null)
                initEvent(eventAction);

    }

    /**
     * Convert data to param-key
     *
     * @return JSonObject
     */

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public JSONObject getJSONObjectData() {
        JSONObject param = new JSONObject();
        try {
            param.put("uid", getUID());
            param.put("aid", getUUID());
            param.put("sid", getSectionId());
            param.put("ea",eventActionName);
            param.put("ec", eventCategory);

            param.putOpt("context", contextModel.getContextModel());

            if (getDimension() != null)
                param.putOpt("dims", getDimension());

            if (productItemList != null && productItemList.size() > 0)
                param.putOpt("items", getProducts());
            else if (userItem != null)
                param.putOpt("items", getUser());

            if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
                JSONObject extraParam = new JSONObject();
                extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                param.putOpt("extra", extraParam);
                InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
            } else {
                if (extraItem != null)
                    param.putOpt("extra", getExtraItem());

                if (eventAction.equals(Events.IDENTIFY) && !TextUtils.isEmpty(userItem.getUserInfo().toString())
                        && !"reset_anonymous_id".equals(eventActionName)) {
                    InsightSharedPref.savePreference(Constants.PREF_IS_PUSH_PLAYER_ID, true);
                    param.putOpt("extra", userItem.getUserInfo());
                }
                if ("reset_anonymous_id".equals(eventActionName)) {
                    AnonymousItem anonymousItem = new AnonymousItem(getUID(), getNewUID());
                    param.put("extra", anonymousItem.getAnonymousItem());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    public List<ProductItem> getProductItemList() {
        return productItemList;
    }

    private String getUID() {
        String uid;
        if (getAnonymousIndex() != 0)
            uid = getUUID() + "_" + (getAnonymousIndex());
        else
            uid = getUUID();
        return uid;
    }

    private String getNewUID() {
        String uid;
        uid = getUUID() + "_" + (getAnonymousIndex() + 1);
        return uid;
    }

    @SuppressLint("HardwareIds")
    private String getUUID() {
        UUID androidId_UUID = null;
        String androidId = Settings.Secure.getString(CDP365AnalyticSDK.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        try {
            androidId_UUID = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return androidId_UUID != null ? androidId_UUID.toString().toUpperCase() : "";
    }


    private void initEvent(@NonNull Events eventAction) {
        switch (eventAction) {
            case IMPRESSION:
            case ADX_CLICK:
            case VIEWABLE:
                eventCategory = Constants.ADVERTISING_CATEGORY;
                eventActionName = eventAction.getValue();
                break;
            case IDENTIFY:
            case RESET_ANONYMOUS_ID:
            case SIGN_OUT:
                    eventCategory = Constants.USER_IDENTIFY_CATEGORY;
                    eventActionName = eventAction.getValue();
                break;
            case SIGN_IN:
                    eventCategory = Constants.USER_IDENTIFY_CATEGORY;
                    eventActionName = eventAction.getValue();
                InsightSharedPref.savePreference(Constants.PREF_IS_PUSH_PLAYER_ID, true);
                break;
            case SCREEN_VIEW:
                    eventCategory = Constants.SCREEN_VIEW_CATEGORY;
                    eventActionName = eventAction.getValue();
                break;

            case PRODUCT_SEARCH:
            case PRODUCT_LIST_FILTER:
            case PRODUCT_LIST_VIEW:
                eventCategory = Constants.BROWSING_CATEGORY;
                eventActionName = eventAction.getValue();
                break;

            case VIEW_PRODUCT_DETAIL:
            case REMOVE_CART:
            case PURCHASE:
            case CHECKOUT:
            case PRODUCT_CLICK:
            case VIEW_COMPARE_LIST:
            case PAYMENT:
            case ADD_COMPARE:
            case ADD_WISH_LIST:
            case VIEW_WISH_LIST:
            case ADD_TO_CART:
            case VIEW_CART:
                    eventCategory = Constants.PRODUCT_CATEGORY;
                    eventActionName = eventAction.getValue();
                break;
            case SIGN_UP:
            case COUPON_LIST_VIEW:
            case COUPON_ADD_FAVORITE:
            case COUPON_REDEEM:
            case COUPON_REDEEM_COMPLETE:
            case POINT_EARNING:
                break;
            default:
                break;
        }
    }

    private JSONObject getExtraItem() {
        return extraItem.getExtraData();
    }

    private JSONArray getProducts() {
        if (productItemList == null)
            return null;
        JSONArray array = new JSONArray();
        for (ProductItem productItem : productItemList) {
            array.put(productItem.getProduct());
        }
        return array;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private JSONArray getUser() {
        if (userItem == null)
            return null;
        List<UserItem> userList = new ArrayList<>();
        userList.add(userItem);
        JSONArray array = new JSONArray();
        for (UserItem userItem : userList) {
            if (userItem.getUserInfo() != null)
                array.put(userItem.getUserInfo());
        }
        return array;
    }

    public void setCampaign(Campaign campaign) {
        if (contextModel != null)
            contextModel.setCampaign(campaign);
    }

    private JSONObject getDimension() {
        if (dimensionList == null)
            return null;
        JSONObject dimensionObject = new JSONObject();
        for (Dimension dimension : dimensionList) {
            try {
                dimensionObject.putOpt(dimension.getDimensionCategory(), dimension.getDimensionObject());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return dimensionObject;
    }

    private int getAnonymousIndex() {
        int index = 0;
        String indexFromFile = Anonymous.getInstance().getIndexFromStorageLocal(mContext);
        if (TextUtils.isEmpty(indexFromFile)) {
            return 0;
        } else {
            try {
                index = Integer.parseInt(indexFromFile);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return index;
    }

    public void updateAnonymousIndex() {
        int newIndex = getAnonymousIndex() + 1;
        Anonymous.getInstance().saveIndexToStorageLocal(mContext, String.valueOf(newIndex));
    }

    private String getSectionId() {
        if (TextUtils.isEmpty(sections)) sections = getSections();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                sections = getSections();
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, INTERVAL_REFRESH_SECTION);
        return sections;
    }

    private static String getSections() {
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmm", Locale.getDefault());
        return formatter.format(todayDate);
    }
}
