package ants.mobile.ants_insight.Model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import ants.mobile.ants_insight.Anonymous;
import ants.mobile.ants_insight.Constants.Constants;
import ants.mobile.ants_insight.Constants.Events;
import ants.mobile.ants_insight.CDP365AnalyticSDK;
import ants.mobile.ants_insight.InsightSharedPref;

import static ants.mobile.ants_insight.Constants.Constants.PREF_ANDROID_APP_PUSH_ID;
import static ants.mobile.ants_insight.Constants.Constants.PREF_IS_FIRST_INSTALL_APP;

public class AnalyticDataRequest {
    private ContextModel contextModel;
    private static final Long INTERVAL_REFRESH_SECTION = 1800000L; // milli second
    private Context mContext;
    private Events eventAction;
    private String eventCategory;
    private String eventActionName;

    public JSONArray itemArray;
    public JSONObject extraObject;
    public JSONArray dimensionArray;
    private static String sections = "";

    public static class Builder {
        private Context context;
        private JSONArray itemArray;
        private JSONObject extraObject;
        public JSONArray dimensionArray;
        private Events eventAction;
        private String eventCategory;
        private String eventActionName;

        public Builder eventAction(Events customEvent){
            this.eventAction = customEvent;
            return this;
        }
        public Builder eventAction(String customEvent){
            this.eventActionName = customEvent;
            return this;
        }
        public Builder eventCategory(String eventCategory){
            this.eventCategory = eventCategory;
            return this;
        }
        public Builder itemList(JSONArray itemArray) {
            this.itemArray = itemArray;
            return this;
        }
        public Builder extraItem(JSONObject extraObject) {
            this.extraObject = extraObject;
            return this;
        }
        public Builder dimensionList(JSONArray dimensionList) {
            this.dimensionArray = dimensionList;
            return this;
        }
        public Builder setInstance(Context context) {
            this.context = context;
            return this;
        }
        public AnalyticDataRequest build() {
            return new AnalyticDataRequest(this);
        }
    }

    private AnalyticDataRequest(Builder builder) {
        this.mContext = builder.context;
        this.contextModel = new ContextModel(builder.context);
        this.eventAction = builder.eventAction;
        this.eventCategory = builder.eventCategory;
        this.eventActionName = builder.eventActionName;
        this.itemArray = builder.itemArray;
        this.extraObject = builder.extraObject;
        this.dimensionArray = builder.dimensionArray;
        if (eventAction != null) initEvent(eventAction);
    }

    /**
     * Convert data to param-key
     *
     * @return JSonObject
     */

//    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//    public JSONObject getJSONObjectData() {
//        JSONObject param = new JSONObject();
//        try {
//            param.put("uid", getUID());
//            param.put("aid", getUUID());
//            param.put("sid", getSectionId());
//            param.put("ea",eventActionName);
//            param.put("ec", eventCategory );
//            param.putOpt("context", contextModel.getContextModel());
//            if (itemCustomList != null && itemCustomList.size() > 0){
//                param.putOpt("items", getItems());
//                if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
//                    JSONObject extraParam = new JSONObject();
//                    extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
//                    param.putOpt("extra", extraParam);
//                    InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
//                } else {
//                    if (customExtraItem != null){
//                        if (eventAction == Events.SIGN_UP)
//                            customExtraItem.setType("link_card");
//                        param.putOpt("extra", getExtras());
//                    }
//                }
//            }
//            else {
//                if (customExtraItem != null){
//                    if (eventAction == Events.SIGN_UP)
//                        customExtraItem.setType("no_card");
//                    param.putOpt("extra", getExtras());
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return param;
//    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public JSONObject getJSONObjectData() {
        JSONObject param = new JSONObject();
        try {
            param.put("uid", getUID());
            param.put("aid", getUUID());
            param.put("sid", getSectionId());
            param.put("ea",eventActionName);
            if (eventCategory!=null) param.put("ec", eventCategory);
            param.putOpt("context", contextModel.getContextModel());
            if (dimensionArray!=null) param.putOpt("dims",dimensionArray);
            if (itemArray !=null){
                param.putOpt("items", itemArray);
                if (InsightSharedPref.getBooleanValue(PREF_IS_FIRST_INSTALL_APP)) {
                    JSONObject extraParam = new JSONObject();
                    extraParam.put("android_app_push_id", InsightSharedPref.getStringValue(PREF_ANDROID_APP_PUSH_ID));
                    param.putOpt("extra", extraParam);
                    InsightSharedPref.savePreference(PREF_IS_FIRST_INSTALL_APP, false);
                }
                else {
                    if (extraObject!=null)
                        param.putOpt("extra", extraObject);
                }
            }else {
                if (extraObject!=null)
                    param.putOpt("extra", extraObject);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private String getUID() {
        String uid;
        if (getAnonymousIndex() != 0)
            uid = getUUID() + "_" + (getAnonymousIndex());
        else
            uid = getUUID();
        return uid;
    }

    private String getNewUID() {
        String uid;
        uid = getUUID() + "_" + (getAnonymousIndex() + 1);
        return uid;
    }

    @SuppressLint("HardwareIds")
    private String getUUID() {
        UUID androidId_UUID = null;
        String androidId = Settings.Secure.getString(CDP365AnalyticSDK.getInstance().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        try {
            androidId_UUID = UUID.nameUUIDFromBytes(androidId.getBytes("utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return androidId_UUID != null ? androidId_UUID.toString().toUpperCase() : "";
    }

    private void initEvent(@NonNull Events eventAction) {
        switch (eventAction) {
            case IDENTIFY:
            case SIGN_OUT:
            case SIGN_IN:
                eventCategory = Constants.USER_IDENTIFY_CATEGORY_CUSTOM;
                eventActionName = eventAction.getValue();
                InsightSharedPref.savePreference(Constants.PREF_IS_PUSH_PLAYER_ID, true);
                break;
            case SCREEN_VIEW:
                    eventCategory = Constants.SCREEN_VIEW_CATEGORY_CUSTOM;
                    eventActionName = eventAction.getValue();
                break;
            case PRODUCT_SEARCH:
                    eventCategory = Constants.STORE_CATEGORY_CUSTOM;
                    eventActionName = "search";
                break;
            case VIEW_CART:
                eventCategory = Constants.CART_CUSTOM;
                eventActionName = "view";
                break;
            case SIGN_UP:
                eventCategory = Constants.USER_IDENTIFY_CATEGORY_CUSTOM;
                eventActionName = eventAction.getValue();
                break;
            case VIEW_PRODUCT_DETAIL:
            case COUPON_LIST_VIEW:
            case COUPON_ADD_FAVORITE:
            case COUPON_REDEEM:
            case COUPON_REDEEM_COMPLETE:
                    eventCategory = Constants.COUPON_CUSTOM;
                    eventActionName = eventAction.getValue();
                break;
            case POINT_EARNING:
                    eventCategory = Constants.POINT_CUSTOM;
                    eventActionName =  eventAction.getValue();
                break;
            default:
                break;
        }
    }
    private int getAnonymousIndex() {
        int index = 0;
        String indexFromFile = Anonymous.getInstance().getIndexFromStorageLocal(mContext);
        if (TextUtils.isEmpty(indexFromFile)) {
            return 0;
        } else {
            try {
                index = Integer.parseInt(indexFromFile);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return index;
    }
    private String getSectionId() {
        if (TextUtils.isEmpty(sections)) sections = getSections();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                sections = getSections();
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, INTERVAL_REFRESH_SECTION);
        return sections;
    }
    private static String getSections() {
        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("yyMMddHHmm", Locale.getDefault());
        return formatter.format(todayDate);
    }
}
