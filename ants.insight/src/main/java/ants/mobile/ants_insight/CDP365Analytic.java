package ants.mobile.ants_insight;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ants.mobile.ants_insight.Constants.Constants;
import ants.mobile.ants_insight.Constants.Events;
import ants.mobile.ants_insight.Model.AnalyticDataRequest;
import ants.mobile.ants_insight.Model.Dimension;
import ants.mobile.ants_insight.Model.ExtraItem;
import ants.mobile.ants_insight.Model.ProductItem;
import ants.mobile.ants_insight.Model.UserItem;
import ants.mobile.ants_insight.Service.GoogleTracking;
import ants.mobile.ants_insight.adx.ActivityLifecycleListener;
import ants.mobile.ants_insight.adx.Campaign;
import ants.mobile.ants_insight.adx.Utils;
import ants.mobile.ants_insight.adx.WebViewManager;
import ants.mobile.ants_insight.Model.CurrentLocation;
import ants.mobile.ants_insight.Response.DeliveryResponse;
import ants.mobile.ants_insight.Model.InsightConfig;
import ants.mobile.ants_insight.Model.InsightDataRequest;
import ants.mobile.ants_insight.Service.ApiClient;
import ants.mobile.ants_insight.Service.DeliveryApiDetail;
import ants.mobile.ants_insight.Service.InsightApiDetail;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CDP365Analytic {

    private Context mContext;
    private InsightApiDetail isApiDetail;
    private DeliveryApiDetail deliveryApiDetail;
    private static final String TAG = CDP365Analytic.class.getCanonicalName();
    private JsonObject paramObject;
    private boolean isShowInAppView = true;
    private boolean isDelivery = true;

    private String[] PERMISSIONS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.CAMERA};

    public static class Builder {
        private Context mContext;

        public CDP365Analytic.Builder setInstance(Context context) {
            this.mContext = context;
            return this;
        }

        public CDP365Analytic build() {
            return new CDP365Analytic(this);
        }
    }

    private CDP365Analytic(Builder builder) {
        mContext = builder.mContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            initialization();

        readConfigFile();
    }
    /**
     * get config in asset and read config file
     */
    private void readConfigFile() {
//        if (TextUtils.isEmpty(InsightSharedPref.getStringValue(Constants.PREF_PROPERTY_ID)) ||
//                TextUtils.isEmpty(InsightSharedPref.getStringValue(Constants.PREF_PORTAL_ID))) {
            String data = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                data = Utils.getAssetJsonData(mContext);
            }
            Type type = new TypeToken<InsightConfig>() {
            }.getType();
            InsightConfig config = new Gson().fromJson(data, type);

            if (validConfigFile(config))
                saveConfigToSharedPref(config);
            else
                Log.e(TAG, "PLEASE READ THE CONFIGURATION FILE CREATION GUIDE AGAIN");
//        }
    }
    /**
     * instance app lifecycle when pause app or save state of app
     * save location of
     * get Instance variable for delivery app and call API of delivery app
     */
    private void initialization() {
        ActivityLifecycleListener.registerActivityLifecycleCallbacks((Application) mContext.getApplicationContext());

        isDelivery = InsightSharedPref.getBooleanValue(Constants.IS_DELIVERY);

        if (Utils.getActivity(mContext) != null) {
            new CurrentLocation.Builder().activity(Utils.getActivity(mContext)).build().getAndSaveLastLocation();
            if (!hasPermissions(mContext, PERMISSIONS))
                ActivityCompat.requestPermissions(Utils.getActivity(mContext), PERMISSIONS, Constants.PERMISSION_ALL);
        }

    }

    public void setIsShowInAppView(boolean isShowInAppView) {
        this.isShowInAppView = isShowInAppView;
    }


    /**
     * Call when using the event: first install app
     * dowload file config and save to shared Preference
     */
    public void firstInstallApp(String playerId) {
        InsightSharedPref.savePreference(Constants.PREF_IS_FIRST_INSTALL_APP, true);
        InsightSharedPref.savePreference(Constants.PREF_ANDROID_APP_PUSH_ID, playerId);
        InsightDataRequest data = new InsightDataRequest.Builder().eventName(Events.IDENTIFY).setInstance(mContext).build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            paramObject = (JsonObject) JsonParser.parseString(data.getJSONObjectData().toString());
        isApiDetail.logEvent(getQueryParam(Constants.INSIGHT), paramObject);
        if (isDelivery) {
            deliveryApiDetail.logDelivery(getQueryParam(Constants.DELIVERY), paramObject);
        }
    }

    //user for screen view
    public void logEvent(@NonNull String actionEvent) {
        AnalyticDataRequest data = new AnalyticDataRequest.Builder().eventAction(actionEvent)
                .setInstance(mContext).build();
        //        callApiString(data);
        callApiAsync(data);
    }
    public void logEvent(@NonNull Events actionEvent) {
        AnalyticDataRequest data = new AnalyticDataRequest.Builder().eventAction(actionEvent)
                .setInstance(mContext).build();
        //        callApiString(data);
        callApiAsync(data);
    }
    public void logEvent(@NonNull String eventCategory,@NonNull String actionEvent, @Nullable JSONArray productItems,
                               @Nullable JSONObject extraItem,@Nullable JSONArray dimension) {
        AnalyticDataRequest data = new AnalyticDataRequest.Builder().eventAction(actionEvent)
                .eventCategory(eventCategory)
                .itemList(productItems)
                .extraItem(extraItem)
                .dimensionList(dimension)
                .setInstance(mContext).build();
//        callApiString(data);
        callApiAsync(data);


    }
    private void logEventGoogleFaceBook(@NonNull Events actionEvent, @NonNull List<ProductItem> productItems) {
        InsightDataRequest data = new InsightDataRequest.Builder().eventName(actionEvent)
                .productList(productItems)
                .setInstance(mContext).build();
//        callApi(data);
        callApiGoogleFacebook(data);
    }

    private void callApiGoogleFacebook(InsightDataRequest data) {
        if (isNetworkAvailable(mContext)){
            // eventName equals: purchase, add_to_cart, view_product, search, checkout then fb API
            // eventName in SDK not matching fb API
            Thread thread = new Thread(){
                @Override
                public void run() {
                    if(InsightDataRequest.isCustomEvent != true){
                        checkEvents(data.eventAction);
                        FacebookEvents fb = new FacebookEvents.Builder().insightEventName(data.eventAction)
                                .setProductList(data.getProductItemList()).build();

                        GoogleTracking googleTracking = new GoogleTracking.Builder().insightEventName(data.eventAction)
                                .setProductList(data.getProductItemList()).build();

                        googleTracking.googleTrackingEvent();
                        fb.callApiFacebook();
                    }
                    else    Log.d("<<<<<<INSIGHT_SDK", "No connection");
                }
            };
            thread.start();
        }
    }

    void callApiAsync(AnalyticDataRequest data){
        deliveryApiDetail = ApiClient.getDeliveryInstance();
        isApiDetail = ApiClient.getInsightInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            paramObject = (JsonObject) JsonParser.parseString(data.getJSONObjectData().toString());
        AsyncCallApi asyncCallApi = new AsyncCallApi();
        asyncCallApi.execute();
    }

    private class AsyncCallApi extends AsyncTask<JsonObject,JsonObject,JsonObject>{
        @Override
        protected JsonObject doInBackground(JsonObject... jsonObjects) {
            try{
                if(isNetworkAvailable(mContext)){
                Log.d("<<<<<<INSIGHT_SDK_Async", paramObject.toString());
                isApiDetail.logEvent(getQueryParam(Constants.INSIGHT), paramObject)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe();
                if (isDelivery) {
                    deliveryApiDetail.logDelivery(getQueryParam(Constants.DELIVERY), paramObject)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new CustomApiCallBack<DeliveryResponse>() {
                                @Override
                                public void onNext(DeliveryResponse response) {
                                    super.onNext(response);
                                    //Todo: handle show ads
                                    if (isShowInAppView && response.campaignStatus() && response.getCampaign() != null && response.getCampaign().get(0) != null) {
                                        response.getCampaign().get(0).setPositionId(response.getCampaign().get(0).getPositionId());
                                        response.getCampaign().get(0).setNative(false);
                                        handleShowAd(response.getCampaign().get(0));
                                    }

                                }
                            });
                }
            }
            else {
                Log.d("<<<<<<INSIGHT_SDK", "No connection");

            }
            }catch (Exception e){
                e.printStackTrace();
            }

        return null;
    }
    }
    /**
     * Check network state
     * @param activity context of app
     * @return
     */
    private static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;  //<--  --  -- Connected
                    }
                }
            }
        }
        return false;  //<--  --  -- NOT Connected
    }
    /**
     * Read parameter from config file and parse data of config file
     * handle display ads
     * check event has been called. if event called matched with google and fb event send data to google and fb
     * @param dataRequest
     */
    private void callApi(InsightDataRequest dataRequest) {
        if(isNetworkAvailable(mContext)){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                paramObject = (JsonObject) JsonParser.parseString(dataRequest.getJSONObjectData().toString());
                Log.d("<<<<<<INSIGHT_SDK", paramObject.toString());
            isApiDetail.logEvent(getQueryParam(Constants.INSIGHT), paramObject)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe();
            if (isDelivery) {
                deliveryApiDetail.logDelivery(getQueryParam(Constants.DELIVERY), paramObject)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe(new CustomApiCallBack<DeliveryResponse>() {
                            @Override
                            public void onNext(DeliveryResponse response) {
                                super.onNext(response);
                                //Todo: handle show ads
                                if (isShowInAppView && response.campaignStatus() && response.getCampaign() != null && response.getCampaign().get(0) != null) {
                                    response.getCampaign().get(0).setPositionId(response.getCampaign().get(0).getPositionId());
                                    response.getCampaign().get(0).setNative(false);
                                    handleShowAd(response.getCampaign().get(0));
                                }
                            }
                        });
            }
            // eventName equals: purchase, add_to_cart, view_product, search, checkout then fb API
            // eventName in SDK not matching fb API
            if(InsightDataRequest.isCustomEvent != true){
                checkEvents(dataRequest.eventAction);
                FacebookEvents fb = new FacebookEvents.Builder().insightEventName(dataRequest.eventAction)
                        .setProductList(dataRequest.getProductItemList()).build();

                GoogleTracking googleTracking = new GoogleTracking.Builder().insightEventName(dataRequest.eventAction)
                        .setProductList(dataRequest.getProductItemList()).build();

                googleTracking.googleTrackingEvent();
                fb.callApiFacebook();
            }
        }
        else  Log.d("<<<<<<INSIGHT_SDK", "No connection");
    }


    /**
     * get query param
     *
     * @param type : delivery or insight
     * @return Map<String, String>
     */
    private Map<String, String> getQueryParam(int type) {
        Map<String, String> param = new HashMap<>();
        if (type == Constants.DELIVERY)
            param.put("format", "json");
        else
            param.put("resp_type", "json");
        param.put("portal_id", InsightSharedPref.getStringValue(Constants.PREF_PORTAL_ID));
        param.put("prop_id", InsightSharedPref.getStringValue(Constants.PREF_PROPERTY_ID));
        return param;
    }

    /**
     * check event will be called from logEvent()
     * if event input matching with event of list<Events> return true
     * else return false
     * @param event
     * @return
     */
    private boolean checkEvents(Events event) {
        List<Events> eventsAds = new ArrayList<>();
        eventsAds.add(Events.PURCHASE);
        eventsAds.add(Events.ADD_TO_CART);
        eventsAds.add(Events.PAYMENT);
        eventsAds.add(Events.VIEW_PRODUCT_DETAIL);
        eventsAds.add(Events.PRODUCT_SEARCH);

        String asdsad= event.getValue();
        Log.d(TAG, "checkEvents: "+ asdsad);

        for (int i = 0; i < eventsAds.size(); i++) {
            if (eventsAds.get(i).getValue().equals(event.getValue()))
                return true;
        }
        return false;
    }

    /**
     * receive config from server and display ads
     * @param campaign
     */
    private void handleShowAd(Campaign campaign) {
        if (!WebViewManager.isShowingAds) {
            WebViewManager.showHTMLString(campaign);
            WebViewManager.isShowingAds = true;
        }
    }

    /**
     * reset if anonymous close app
     */
    public void resetAnonymousId() {
        AnalyticDataRequest param = new AnalyticDataRequest.Builder().eventCategory("user")
                .eventAction("reset_anonymous_id").setInstance(mContext).build();
        if (!Anonymous.getInstance().isFileExists()) {
            Anonymous.getInstance().saveIndexToStorageLocal(mContext, "0");
            InsightSharedPref.savePreference(Constants.PREF_UID, "0");
        }

        callApiAsync(param);
    }

    /**
     * save config from asset to shared pref
     * @param config
     */
    private static void saveConfigToSharedPref(InsightConfig config) {
        InsightSharedPref.savePreference(Constants.PREF_INSIGHT_URL, config.getInsightUrl());
        InsightSharedPref.savePreference(Constants.PREF_DELIVERY_URL, config.getDeliveryUrl());
        InsightSharedPref.savePreference(Constants.IS_DELIVERY, config.isDelivery());
        InsightSharedPref.savePreference(Constants.PREF_PORTAL_ID, config.getPortalId());
        InsightSharedPref.savePreference(Constants.PREF_PROPERTY_ID, config.getPropertyId());
        InsightSharedPref.savePreference(Constants.PREF_FB_ADD_TO_CART_APP_ID, config.getFbAddToCartAppId());
        InsightSharedPref.savePreference(Constants.PREF_FB_PURCHASE_TOKEN, config.getFbPurchaseToken());
        InsightSharedPref.savePreference(Constants.PREF_FB_ADD_TO_CART_TOKEN, config.getFbAddToCartToken());
        InsightSharedPref.savePreference(Constants.PREF_FB_CHECK_OUT_TOKEN, config.getFbCheckoutToken());
        InsightSharedPref.savePreference(Constants.PREF_FB_VIEW_PRODUCT_TOKEN, config.getFbViewProductToken());
        InsightSharedPref.savePreference(Constants.PREF_FB_CHECKOUT_APP_ID, config.getFbCheckOutAppId());
        InsightSharedPref.savePreference(Constants.PREF_FB_PURCHASE_APP_ID, config.getFbPurchaseAppId());
        InsightSharedPref.savePreference(Constants.PREF_FB_VIEW_PRODUCT_APP_ID, config.getFbViewProductAppId());
        InsightSharedPref.savePreference(Constants.PREF_GG_DEV_TOKEN, config.getDevToken());
        InsightSharedPref.savePreference(Constants.PREF_GG_VIEW_PRODUCT_LINK_ID, config.getGgViewProductLinkId());
        InsightSharedPref.savePreference(Constants.PREF_GG_VIEW_LIST_LINK_ID, config.getGgViewListLinkId());
        InsightSharedPref.savePreference(Constants.PREF_GG_PURCHASE_LINK_ID, config.getGgPurchaseLinkId());
        InsightSharedPref.savePreference(Constants.PREF_GG_ADD_TO_CART_LINK_ID, config.getGgAddToCartLinkId());
    }

    /**
     * check config file exist or not
     * if config null return false then call func saveConfigToSharedPref to save file
     * else check PortalId and PropertyId null or not
     * @param config
     * @return
     */
    private static boolean validConfigFile(InsightConfig config) {
        if (config == null)
            return false;
        else {
            return !TextUtils.isEmpty(config.getPortalId()) ||
                    !TextUtils.isEmpty(config.getPropertyId());
        }
    }

    /**
     * check Android permissions
     * @param context
     * @param permissions
     * @return
     */
    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
