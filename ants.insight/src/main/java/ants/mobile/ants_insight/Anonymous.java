package ants.mobile.ants_insight;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * this class will be called when user not login with user account.
 * system will create default account for user
 * it will be lost when close app
 */
public class Anonymous {

    private static Anonymous ourInstance = null;

    private Anonymous() {
    }

    public static Anonymous getInstance() {
        if (ourInstance == null) {
            ourInstance = new Anonymous();
        }
        return (ourInstance);
    }
    public void  saveIndexToStorageLocal(Context mContext, String data){
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(mContext.openFileOutput("index.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public boolean isFileExists() {
        File directory = new File( CDP365AnalyticSDK.getInstance().getApplicationContext().getFilesDir().toString());
        if (!directory.exists()) {
            directory.mkdir();
            return false;
        }
        File indexFile = new File(directory, "index.txt");
        return indexFile.exists();
    }

    public  String getIndexFromStorageLocal(Context context){
        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("index.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

}