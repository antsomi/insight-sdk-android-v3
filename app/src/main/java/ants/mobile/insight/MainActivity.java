package ants.mobile.insight;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ants.mobile.CDP365Analytic.R;
import ants.mobile.ants_insight.CDP365Analytic;
import ants.mobile.ants_insight.Constants.Events;

public class MainActivity extends Activity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CDP365Analytic cdp365Analytic = new CDP365Analytic.Builder().setInstance(this).build();
        try {
        JSONObject jo = new JSONObject();
            jo.put("item_id", "CARD_NUMBER");
            jo.put("item_name", "Card Name");
            jo.put("item_type", "member_card");
            JSONArray jsonUserItem = new JSONArray();
            jsonUserItem.put(jo);

            JSONObject extraObject = new JSONObject();
            extraObject.put("earning_point_type","Invite_friend");
            extraObject.put("point_added","POINT_ADDE_NUMBER");
            extraObject.put("customer_id","CARD_NUMBER_ID");

            JSONObject jo1 = new JSONObject();
            jo1.put("item_color", "Red");
            jo1.put("item_info", "xyz");
            jo1.put("item_size", "width: 30, height: 50");
            JSONArray dimensionObject = new JSONArray();
            dimensionObject.put(jo1);

        cdp365Analytic.logEvent("user","reset_anonymous",jsonUserItem,extraObject,dimensionObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}